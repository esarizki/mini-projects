import Link from 'next/link'
import { useSession, signIn, signOut } from "next-auth/react"
import Button from './button.components'
export function Navbar() {        
    return (
        <nav className={"navbar navbar-expand-lg navbar-dark bg-dark"}>
            <a className={"navbar-brand"} href="#">Navigation</a>
            
            <button className={"navbar-toggler"} type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className={"navbar-toggler-icon"}></span>
            </button>

            <div className={"collapse navbar-collapse"} id="navbarSupportedContent">
                <ul className={"navbar-nav mr-auto"}>
                <li className={"nav-item active"}>
                    <Link className={'nav-link'} href='/'>Home</Link>                    
                </li>
                <li className={"nav-item"}>
                    <Link className={'nav-link'} href='/ssr'>SSR</Link>                    
                </li>                
                <li className={"nav-item"}>
                    <Link className={'nav-link'} href='/fetching/ssg'>SSG</Link>                    
                </li>                        
                <li className={"nav-item"}>
                    <Link className={'nav-link'} href='/dynamic/1'>Dynamic Pages</Link>                    
                </li>   
                <li className={"nav-item"}>
                    <Link className={'nav-link'} href='/auth/private'>Private Route</Link>                    
                </li>   
                <li className={"nav-item"}>
                    <Link className={'nav-link'} href='/crud'>CRUD</Link>                    
                </li>                     
                <li className={"nav-item"}>
                    <a className={'nav-link'} onClick={() => signIn()}>Login</a>                    
                </li>                                
                <li className={"nav-item"}>
                    <a className={'nav-link'} onClick={() => signOut()} >Logout</a>                    
                </li>                                                    
                </ul>    
            </div>       
            <p className={'navbar-text'}>Hello {''} </p>     
            
        </nav>
    )
};

