import PropTypes from "prop-types";
function Button(props) {
    return <button        
        type={props?.htmlType}
        onClick={props?.onClick}
        className={props?.className}
    >{props?.children}</button>
};

Button.propTypes = {
    htmlType: PropTypes.oneOf(['button', 'submit']).isRequired,
    type: PropTypes.oneOf(['primary', 'default']),
    className: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    htmlType: 'button',
    type: 'default',
    className: 'btn-info',
    onClick: function(events) {
        console.log(events);
    }
}

export default Button

