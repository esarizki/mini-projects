import { Navbar } from '@/components/navbar'
import styles from '@/styles/Home.module.css'
export default function Dynamic(props) {
    var data = props?.data
    return (
        <>
        <Navbar/>
        <main className={styles.main}>                                
            <table className={'table table-sm'}>
                <caption>UserData Detail { props?.id}</caption>
                <thead>
                <tr>
                    <th>userID</th>
                    <th>id</th>
                    <th>title</th>
                    <th>body</th>
                </tr>
                <tr>
                    <th>{data.userId}</th>
                    <th>{data.id}</th>
                    <th>{data.title}</th>
                    <th>{data.body}</th>
                </tr>
                </thead>                
            </table>
        </main>
        </>
    )
};

export async function getServerSideProps(context) {
    let { id } = context.params
    let data = []

    await fetch('https://jsonplaceholder.typicode.com/posts/'+id)
      .then((response) => response.json())
      .then((response) => {
          data = response
      })
      .catch((err) => {
          data = []
      })

    // console.log(context.params);

    return {
        props: {
            id,
            data
        }
    }
}