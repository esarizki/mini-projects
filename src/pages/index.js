import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { Navbar } from '@/components/navbar'
import { useSession } from 'next-auth/react'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {  
  return (
    <>      
    <Navbar/>
      <main className={styles.main}>                      
        <div className={styles.center}>
          <Image
            className={styles.logo}
            src="/next.svg"
            alt="Next.js Logo"
            width={180}
            height={37}
            priority
          />
          <div >
            <h2 className={inter.className} > ESA</h2>
            <h4 className={inter.className} > MINI PROJECT</h4>
          </div>          
        </div>        
      </main>
    </>
  )
}
