import ErrorHandler from '@/handlers/error.handler'
import ProductController from "@/controller/product.controller";
import nc from 'next-connect'
import {isNumber} from 'lodash'

const handler = nc(ErrorHandler)

handler
    .post(async (req,res) => {
        let recordData = req.body;

        // create user baru
        const [err,data] = await new ProductController({
            fields: recordData
        }).create()

        if(err){
            return res.status(400).json({
                message: err?.message ?? "Error: Some Error"
            })
        }
        return res.status(200).json({
            message: 'OK!',
            data: data
        })
    })
    .get(async (req, res) => {                
        const [err, data] = await new ProductController().get()        
        if (err) {
            res.status(400).json({
                error: true,
                message: "Error guys"
            })
        }        
        return res.status(200).json(data)
    })
    .delete(async (req,res)=>{
        let recordData = req.body
        const [err,data] = await new ProductController({
            key: recordData?.key ?? 'id',
            value: isNumber(recordData?.value) ? Number(recordData?.value) : recordData?.valueOf() ?? null
        }).delete()

        if(err){
            return res.status(400).json({
                message: err?.message ?? "Error: Some Error"
            })
        }

        return res.status(200).json({
            message: 'OK!',
            data: data
        })
    })

export default handler