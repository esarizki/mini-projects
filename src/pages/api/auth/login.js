// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import nc from "next-connect";
import ErrorHandler from "@/handlers/error.handler";

const UserData = {
    username:'esa',
    email:'custom@gmail.com',
    password:'P@ssw0rd'
}

const handler = nc(ErrorHandler)
handler
    .post(async(req,res)=>{
        let recordData = req.body;            
        if(recordData.username==UserData.username && recordData.password==UserData.password)
        {
            return res.status(200).json({
                message: 'OK!',
                data: UserData
            })    
        }else{
            return res.status(500).json({
                message: 'failed Login',
                data: []
            })
        }
        
    })
  
export default handler