import NextAuth from 'next-auth';
import CredentialProvider from 'next-auth/providers/credentials';
import { generateToken } from "@/lig/generateToken";
import moment from 'moment';
import { signIn } from 'next-auth/react';

const UserData = {
  username:'esa',
  email:'rizky.esa@bankmega.com',
  password:'P@ssw0rd'
}

export default NextAuth({
    providers: [
        CredentialProvider({ 
            id: 'credentials',
            name: 'myProject',
            credentials:{
                email: {
                    label: 'email',
                    type: 'email'
                },
                password: {
                    label: 'Password',
                    type: 'password'
                }
            },
            async authorize(credentials, req) {
                let data = {
                  error: true,
                  data: UserData
                }                
                if(credentials.email==UserData.email && credentials.password==UserData.password)
                {
                  data.error=false
                }                
                const token = await generateToken(
                    data?.data,
                    '1d'
                )                
                Reflect.set(
                    data,
                    'token',
                    token)

                // call service
                return{
                    ...data
                }
            }
        })
    ],
    secret: process.env.NEXTAUTH_SECRET,
    jwt: {
        maxAge: 200,
        secret: process.env.NEXTAUTH_SECRET
    },
    session: {
        maxAge: 200,
        strategy: 'jwt'
    },
    pages: {
        signIn: '/auth/login'
    },
    callbacks: {
        async redirect({ url, baseUrl }) {
            return baseUrl
        },
        async signIn({
            account,
            profile,
            user,
            credentials
        }) {
          console.log(account,'account');
            switch (account?.provider) {
                case 'credentials':                  
                    return user?.error === false;
                default:
                    return false;
            }
            
        },
        async jwt({
            token,
            user,
            profile,
            account
        }) {
            user && (token.user = {
                ...user,
                bearer_token: token?.user?.token ?? null,
                id: token?.user?.data?.id ?? null,
                email: token?.user?.data?.email ?? null
            });

            user && (
                token.accessToken = user?.token
            );
            profile && (token.profile = profile)
            account && (
                token.account = account
            )
            return {
                ...token
            }
        },
        async session({
            session,
            token,
            user,
            profile
        }) {
            if (Date.now() > moment(session?.expires)) {
                return null;
            }
            session.user = token?.user
            session.profile = token?.profile ?? null
            session.account = token?.account ?? null
            session.data = token ?? null

            return session
        }
    },
    debug: true
});
