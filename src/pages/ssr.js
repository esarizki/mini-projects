import styles from '@/styles/Home.module.css'
import { Navbar } from '@/components/navbar'
export default function SSR({data}) {
  // console.log(params);
  return (
      <>
      <Navbar/>
      <main className={styles.main}>                    
          <div className={'w-full space-y-6'}>
          <table className={'table table-striped table-hover table-sm'}>
              <thead className={'thead-dark'}>
                <tr>
                  <th>userId</th>
                  <th>id</th>
                  <th>title</th>
                  <th>body</th>
                </tr>
              </thead>
              <tbody>
                {
                    data.map((item,index) => {
                        return (
                            <tr key={index+'ssr'} className={'w-full bg-gray-100 p-4'}>
                                <td>{item.userId}</td>
                                <td>{item.id}</td>
                                <td>{item.title}</td>
                                <td>{item.body}</td>
                            </tr>
                        )
                    })
                }
              </tbody>
          </table>              
          </div>
        </main>
      </>
  )
};

export async function getServerSideProps() {
  let data = []

  await fetch('https://jsonplaceholder.typicode.com/posts')
      .then((response) => response.json())
      .then((response) => {
          data = response
      })
      .catch((err) => {
          data = []
      })

  return {
      props: {
          data
      }
  }
};
