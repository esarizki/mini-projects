import { useFormik } from 'formik'
import { signIn } from 'next-auth/react'
import * as Yup from 'yup'
import {useRouter} from "next/router";
import styles from '@/styles/Home.module.css'

export default function Login() {
    const router = useRouter();

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .max(30, 'Must be 30 characters or less')
                .email('Invalid Email Address')
                .required('please enter your email'),
            password: Yup.string()
                .required('please enter your password')
        }),
        onSubmit: async (value) => {
            
            const credentials = await signIn(
                'credentials',
                {
                    email: value?.email,
                    password: value?.password,
                    redirect: false
                }
            )                      
            if (credentials.ok){
                router.push(
                    credentials.url
                )
            }
            return {
                ...credentials
            }
        }

    })
    return (
        <main className={styles.main}>        
        <section className="bg-gray-50 dark:bg-gray-900">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                
            <h4>Welcome to Mini Project Esa</h4>
                <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">                    
                    <div className="card">
                        <h4 className="card-header">
                            Sign in to your account
                        </h4>
                        <form className={'card-body'} onSubmit={formik.handleSubmit}>
                            <div className={'input-group mb-3'}>
                                <div className={"input-group-prepend"}>
                                    <span className={"input-group-text"} id="basic-addon1">Email</span>
                                </div>
                                <input
                                    className='form-control'
                                        type='email'
                                        name={'email'}
                                        value={formik?.values?.email}
                                        onChange={formik.handleChange}
                                        placeholder={'input your email'}
                                    ></input>
                                    {
                                        formik.errors &&
                                        formik.touched &&
                                        formik.errors?.email &&
                                        formik.touched?.email &&
                                        (
                                            <span className={'!text-red-500 !text-xs'}>{formik.errors?.email}</span>
                                        )
                                    }
                            </div>
                            <div className={'input-group mb-3'}>
                                <div className={"input-group-prepend"}>
                                    <span className={"input-group-text"} id="basic-addon1">Password</span>
                                </div>
                                <input
                                    className='form-control'
                                    type='password'
                                    name={'password'}
                                    value={formik?.values?.password}
                                    onChange={formik.handleChange}
                                    placeholder={'input your password'}
                                ></input>
                                {
                                    formik.errors &&
                                    formik.touched &&
                                    formik.errors?.password &&
                                    formik.touched?.password &&
                                    (
                                        <span className={'!text-red-500 !text-xs'}>{formik.errors?.password}</span>
                                    )
                                }
                            </div>
                            <button
                                type={'submit'}
                                className={'btn btn-info'}
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        </main>
    )
};
