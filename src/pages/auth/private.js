import { Navbar } from '@/components/navbar'
import styles from '@/styles/Home.module.css'
import { useSession, signIn, signOut } from "next-auth/react"


export default function Private(props)
{
    const { data: session, status } = useSession()      
    if(session)
    {
        return (<>
            <Navbar/>
            <main className={styles.main}>                          
                <h4 className={'text-dark'} > WELCOME ESA </h4>            
            </main>
            </>
        )
    }else{
        return (
            <>
            <Navbar/>            
            <main className={styles.main}>     
                <p className={styles.code}>Not signed in</p>    
              <button className={'btn btn-info'} onClick={() => signIn()}>Sign in</button>
            </main>
            </>
          )
    }
    
}