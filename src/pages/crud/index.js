import Button from '@/components/button.components';
import { Navbar } from '@/components/navbar'
import styles from '@/styles/Home.module.css'
import axios from 'axios';
import { useEffect, useState } from 'react'

export default function CRUD(){
    const [data,SetData]= useState([]);          
    const [name,SetName]= useState('');
    const [price,SetPrice]= useState(0);
    const [description,SetDescription]= useState('');
    async function destroydata(id)
    {
        let body = JSON.stringify({
            "id": id,            
          });
          
          let config = {
            method: 'delete',
            maxBodyLength: Infinity,
            url: 'http://localhost:3000/api/product',
            headers: { 
              'Content-Type': 'application/json'
            },
            data : body
          };
          
          axios.request(config)
          .then((response) => {

            SetData(
                [
                    ...data.filter((item)=> item?.id !== id)
                ]
            )
            // alert(id+'deleted')
          })
          .catch((error) => {
            alert('oops there was an error')
          });


    }

    async function inputdata() {
        let body = JSON.stringify({
            "name": name,
            "description": description,
            "price": price
          });
          
          let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'http://localhost:3000/api/product',
            headers: { 
              'Content-Type': 'application/json'
            },
            data : body
          };
          
        axios.request(config)
          .then((response) => {
            console.log(response);
            getdata()            
          })
          .catch((error) => {
            console.log(error);
            alert('oops there was an error')
          });         
           
    }

    async function getdata()
    {
        fetch('/api/product')
      .then((response) => response.json())
      .then((response) => {        
        console.log(response);
          SetData(response)
      })
      .catch((err) => {
        SetData([])
      })
    }

    useEffect(()=>{
      getdata()
    },[])

    return (
        <>
      <Navbar/>      
      <main className={styles.main}>                              
      <div className={'card'}>
            <div className={'card-header'}>Input Data</div>
            <div className={'card-body'}>                                
                <label className={"label"} id="basic-addon1">Name</label>
                <input className='form-control' type='text' name={'name'} onChange={(e)=> SetName(e.target.value)}></input>                                    
                <label className={"label"} id="basic-addon1">price</label>
                <input className='form-control' type='number' onChange={(e)=> SetPrice(e.target.value)} name={'price'}></input>                                    
                <label className={"label"} id="basic-addon1">Description</label>
                <textarea className={'form-control'} onChange={(e)=> SetDescription(e.target.value)}></textarea>                
                <Button className={'btn-info'} onClick={() => inputdata()}>Input</Button>
            </div>            
        </div>      
          <div className={'w-full space-y-6'}>                      
          <table className={'table table-striped table-hover table-sm'}>
            <caption>PRODUCT TABLE</caption>
              <thead className={'thead-dark'}>
                <tr>
                  <th>id</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {
                    data.map((item,index) => {
                        return (
                            <tr key={index+'product'} className={'w-full bg-gray-100 p-4'}>
                                <td>{item.id}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>{item.price}</td>
                                <td><Button className={'btn-danger'} onClick={() =>destroydata(item.id)}>DELETE</Button></td>
                            </tr>
                        )
                    })
                }
              </tbody>
          </table>              
          </div>
        </main>
      </>
    )
}
