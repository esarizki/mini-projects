import '@/styles/globals.css'
import "bootstrap/dist/css/bootstrap.min.css";
// import 'bootstrap/dist/js/bootstrap.js';
import { SessionProvider } from "next-auth/react"
import Head from "next/head";
import { useEffect } from 'react';

export default function App({ Component, pageProps:{ session, ...pageProps} }) {
  return <>
  <Head>
   <meta name="viewport" content="width=device-width, initial-scale=1" />
  </Head>
  <SessionProvider session={session}>
      <Component {...pageProps} />
    </SessionProvider>
  </>  
}
