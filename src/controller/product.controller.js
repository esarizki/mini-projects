import {PrismaClient} from "@prisma/client";

export default class ProductController{
    constructor(props) {
        this.prisma = new PrismaClient()
        this.fields = props?.fields ?? null
        this.key = props?.key ?? null
        this.value = props?.value ?? null
    }

    async create(){
        try {
            if(!this.fields) return [new Error('fields is required'), null]
            console.log(this.fields);
            const  result = await  this.prisma.product.create({
                data: this.fields
            })
            return [null, result]
        }catch (err){
            return [err,null]
        }
    }

    async get(){
        try {            
            const  result = await  this.prisma.product.findMany()
            return [null, result]
        }catch (err){            
            return [err,null]
        }
    }

    async delete(){
        try {
            if(!this.key && !this.value) return [new Error('key & value is required'), null]            
            const deleted = await this.prisma.product.delete({
                where: this.value
            })
            return [null, deleted]
        }catch (e) {
            return [e,null]
        }
    }
}