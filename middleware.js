import {NextResponse} from 'next/server'
import {getToken} from "next-auth/jwt";

export async function middleware(req){
    console.log('tessss')
    console.log(req);
    const token = await getToken({
        req,
        secret: process.env.NEXTAUTH_SECRET,
        secureCookie: false
    })

    console.log(token, 'middleware')

    if(req.nextUrl.pathname.startsWith('/auth')&& token){
        return NextResponse.redirect(new URL('/', req.url))
    }
    if(req.nextUrl.pathname.startsWith('/') && !token){
        return NextResponse.redirect(new URL("/auth/login", req.url))
    }

    return NextResponse.next()
}

export const config = {
    matcher:[
        '/dashboard/:path*', '/dashboard', '/auth', '/auth/:path*'
    ]
}